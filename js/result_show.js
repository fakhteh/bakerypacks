/**
* showing the result of ordering cookies
* result is like:
	<div id=".." class="..">
		<label class=".." id="..">..</label>
		..
		..
	</div>
*/
const resultShow = {
  /**
  * creating div wrapper
  * the first div must be the div including total amount,code,total price with elem_place = 'first'
  * @parentDiv {element by id} parent element
  * @elemId {string}
  * @elemClass {string}
  * @elem_place {string} //if it is 'first' it goes to the first place of the parent
  */
  divCreate: function(parentDiv,elemId,elemClass,elem_place) {
    	var div = document.createElement("div");
		div.id  = elemId;
		div.className = elemClass;
	  	if(elem_place == 'first'){
			parentDiv.insertBefore(div, parentDiv.firstChild);
		}else{
			parentDiv.appendChild(div);
		}
  },
  /**
  * creating labels
  * @div {element by id} parent element
  * @elemId {string}
  * @elemHtml {string}
  * @elemClass {string}
  */	
  lblCreate: function(div,elemId,elemHtml,elemClass) {
    	var label = document.createElement("label");
	  	label.id = elemId;
		label.innerHTML = elemHtml;
		label.className = elemClass;
		
		div.appendChild(label);
  }
}
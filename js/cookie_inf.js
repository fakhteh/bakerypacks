/**
* set cookies information and retrieve data from cookieArr
* @_cookieArr {array}  contains the information about cookies
* @_cookieArr params: @name {string} cookie name.@code {string} cookie code.@packs {array} (count,price).
* @_cookieID  {number}  _cookieArr's key
*/
class getCookies_Inf {
	
	_cookieArr;
	_cookieID;
	
	/**
	* set cookies informations
	*/
	constructor(){
		this._cookieArr = {
			0: {
				name : 'VegemiteScroll',
				code : 'VS5',
				packs : {
					1: {
						count : 5,
						price : '8.99'
					},
					2: {
						count : 3,
						price : '6.99'
					}
				}
			},
			1: {
				name : 'Blueberry Muffin',
				code : 'MB11',
				packs : {
					1: {
						count : 8,
						price : '24.95'
					},
					2: {
						count : 5,
						price : '16.95'
					},
					3: {
						count : 2,
						price : '9.95'
					}
				}
			},
			2: {
				name : 'Croissant',
				code : 'CF',
				packs : {
					1: {
						count : 9,
						price : '16.99'
					},
					2: {
						count : 5,
						price : '9.959'
					},
					3: {
						count : 3,
						price : '5.95'
					}
				}
			}

		}
	}
	
	/**
	* get cookie name from _cookieArr
	* @return {array} cookie's names
	*/
	cookies_name() {
		let cookies_name = [];
		for (const [key, value] of Object.entries(this._cookieArr)) {
			cookies_name.push(value.name);
		}
		return cookies_name;
	}
	
	/**
	* set _cookieID
	* @id {number} _cookieArr's key
	*/
	setCookieID(id) {
		this._cookieID = id;
	}
	
	/**
	* get cookies information according to _cookieID
	* @return {array} selected cookies
	*/
	getCookieInf() {
		return this._cookieArr[this._cookieID];
	}
	
	/**
	* get all cookies information
	* @return {array} all cookies
	*/
	getAllCookiesInf() {
		return this._cookieArr;
	}
}
/**
* cookies_name {array} save name of cookies for dispalying in html dropdown
*/
var cookies_name = [];

$(document).ready(function(){
	getCookiesName();
	showCookieInf();
})

/**
* get cookies name and make dropdown of them.
*/
function getCookiesName() {
	const cookie_obj = new getCookies_Inf();
	cookies_name = cookie_obj.cookies_name();
	
	for (const [key, value] of Object.entries(cookies_name)) {
		var option = document.createElement("option");
		option.innerHTML = value;
		option.value = key;
		
		document.getElementById('cookieId').appendChild(option);  
	}
}
/**
* showing cookies information in the page
* including Name,Code,Packs{count,price}
*/
function showCookieInf() {
	const cookie_obj = new getCookies_Inf();
	let cookies_arr = cookie_obj.getAllCookiesInf();
	
	var td = "";
	for(value of  Object.values(cookies_arr)) {
		var tr = document.createElement("tr");
		
		td = document.createElement("td");
		td.innerHTML = value.name;
		tr.appendChild(td);
		
		td = document.createElement("td");
		td.innerHTML = value.code;
		tr.appendChild(td);
		
		td = document.createElement("td");
		tr.appendChild(td);
		for(packs_value of Object.values(value.packs)) {
			div = document.createElement("div");
			div.innerHTML = `${packs_value.count} @ $ ${packs_value.price}`;
			td.appendChild(div);
		}
	  	
		
		
		document.getElementById('cookies_inf_body').appendChild(tr);
	}
}

function validate_order() {
	let res_flag = true;
	document.getElementById('order_result').innerHTML = '';
	
	/* get inputes from html form */
	let cookieId = document.getElementById('cookieId').value;
	let amount = document.getElementById('amount').value;
	
	if(cookieId == ""){res_flag = false;}
	if(!isNumeric(amount)){res_flag = false;}
	
	if(res_flag){
		submit_order(cookieId,amount);
	}else{
		msg = "<label class='order_res_lbl'><div class = 'div_err_res'>Please select Cookie and amount correctly.</div></label>";
		document.getElementById('order_result').innerHTML = msg;
	}
}
	   
/**
* check if param is number
* @return {boolean}
*/
function isNumeric(value) {
    return /^\d+$/.test(value);
}

/**
* goes to bakery class with total ordering count and objject of cookie
* @cookieId {number}
* @amount {number}
* call resultShow to displaying the result according to ordering result response
*/
function submit_order(cookieId,amount) {
	
	document.getElementById('order_result').innerHTML = '';
	
	const cookie_obj = new getCookies_Inf();
	cookie_obj.setCookieID(cookieId);
	var cookieInf = cookie_obj.getCookieInf();
	
	const bakery_obj = new Bakery();
	bakery_obj.callPacketCounter(amount,cookieInf);
	var order_res = bakery_obj.getResultArr();
	let res_str = "";
	
	var parentDiv = document.getElementById('order_result');
	var elemId = '';
	var elemClass = '';
	var elem_place = '';
	
	var parentLbl = '';
	var lblId = '';
	var lblHtml = ''
	var lblClass = '';
	
	if(order_res == false){
		res_str = "<div class = 'div_err_res'>these packages are not available.</div>";
		
		parentLbl = document.getElementById('order_result');
		lblId = '';
		lblHtml = res_str;
		lblClass = `order_res_lbl`;
		resultShow.lblCreate(parentLbl,lblId,lblHtml,lblClass);
		
	}else{
		var sumPrice = 0; var counter = 0;
		for(Element of order_res){
			var elemPrice = 0;
			
			for(elem of Object.values(cookieInf.packs)){
				if(elem.count == Element.num){
					elemPrice = parseFloat(elem.price);
					sumPrice += parseInt(Element.count)*elemPrice;
				}
			}
			
			elemId = `res_div_${counter}`;
			elemClass = `res_divs pl-5`;
			elem_place = '';
			resultShow.divCreate(parentDiv,elemId,elemClass,elem_place);
			
			parentLbl = document.getElementById(elemId);
			lblClass = `order_res_lbl`;
			
			
			lblId = `res_lbl_cnt_${counter}`;
			lblHtml = Element.count;
			resultShow.lblCreate(parentLbl,lblId,lblHtml,lblClass);
			
			
			lblId = `res_lbl_multi_${counter}`;
			lblHtml = '*';
			resultShow.lblCreate(parentLbl,lblId,lblHtml,lblClass);
			
			
			lblId = `res_lbl_packnum_${counter}`;
			lblHtml = Element.num;
			resultShow.lblCreate(parentLbl,lblId,lblHtml,lblClass);
			
			
			lblId = `res_lbl_total_price_${counter}`;
			lblHtml = '$'+elemPrice;
			resultShow.lblCreate(parentLbl,lblId,lblHtml,lblClass);
			
			counter++;
		}
		
		elemId = `res_div_inf`;
		elemClass = `res_divs`;
		elem_place = 'first';
		resultShow.divCreate(parentDiv,elemId,elemClass,elem_place);

		parentLbl = document.getElementById(elemId);
		lblClass = `order_res_lbl`;
		
		lblId = `res_lbl_total_cnt`;
		lblHtml = amount;
		resultShow.lblCreate(parentLbl,lblId,lblHtml,lblClass);

		
		lblId = `res_lbl_code`;
		lblHtml = cookieInf.code;
		resultShow.lblCreate(parentLbl,lblId,lblHtml,lblClass);

		
		lblId = `res_lbl_total_price`;
		lblHtml = '$'+sumPrice;
		resultShow.lblCreate(parentLbl,elemId,lblHtml,lblClass);
	}
}
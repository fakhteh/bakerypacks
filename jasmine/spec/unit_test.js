const my_cookie =  {
	name : 'VegemiteScroll',
	code : 'VS5',
	packs : {
		1: {
			count : 5,
			price : '8.99'
		},
		2: {
			count : 3,
			price : '6.99'
		}
	}
}
const bakery_obj = new Bakery();


bakery_obj.setCount(8); 

afterEach(function() { 
   bakery_obj.setCount(13); 
});

describe("Check bakery.js packCounter with different params", function() { 

	it("must be true",function() { 
	   expect(bakery_obj).toBeDefined();
	   expect(bakery_obj._packetCounter([9,5,3],0)).toBe(true); 
	  
   }); 
	
   it("must be true",function() { 
	   expect(bakery_obj._packetCounter([8,5,2],0)).toBe(true); 
   });	

});

describe("spy on _packetCounter from _packetCounter", function() { 
   
   it('uses the callPacketCounter to perform "_packetCounter"', function() { 
     	
      spyOn(bakery_obj, "_packetCounter");
      	
      bakery_obj.callPacketCounter(4,my_cookie);
      expect(bakery_obj._packetCounter).toHaveBeenCalled();  
   });  
}); 

const cookie_obj = new getCookies_Inf();
	
describe("cookie_inf", function() { 

	it("must be true",function() { 
		var arr = ["VegemiteScroll","Blueberry Muffin","Croissant"];
	   expect(bakery_obj).toBeDefined();
	   expect(cookies_name = cookie_obj.cookies_name()).toEqual(arr);
		console.log(cookies_name = cookie_obj.cookies_name())
	  
   }); 
	
   	

});

class Bakery {
	_result_arr; 
	_success_flag;
	_count; /* the number of coookies customer wants */
	
	constructor() {
		this._result_arr = []; this._success_flag = true;
	}
	/**
	* performing the whole algorithm by with the recursive function technique
	* @packs {array} array of packes numbers from largest to smallest one
	* @pack_key {number} the key of current packet number that must be used
	* @return {boolean}
	*/
	 _packetCounter(packs,pack_key){
		let packs_lenght = packs.length;let count;
		let packes_cnt = packs[pack_key];
		let n_origin = parseInt(this._count);  packes_cnt = parseInt(packes_cnt);
		let call_again = false; let check_key = -1; let new_key = -1;
		/***********************************/
		let sum = 0;
		for(const [key,value] of this._result_arr){
			sum += parseInt(key)*parseInt(value);
		}
		count = n_origin - parseInt(sum); 
		
		/***********************************/
		while(pack_key < packs_lenght){
			if(count%packes_cnt == 0){
				this._result_arr.push([packes_cnt,count/packes_cnt]);
				break;
			}else{
				if(count < packes_cnt){
					if(pack_key+1 < packs_lenght){
						call_again = true; new_key = ++pack_key;
						break;
					}else{
						var result_arr_length = this._result_arr.length;
						for(var i=0;i<result_arr_length;i++){
							if(this._result_arr[i][1] >0 && check_key == -1){
								this._result_arr[i][1] = (parseInt(this._result_arr[i][1]))-1;
								check_key = i;
							}
						}

						if(check_key == -1){
							this._success_flag = false;
						}else{
							call_again = true; new_key = ++check_key;
							break;
						}
					}
				}else{
					this._result_arr.push([packes_cnt,Math.floor(count/packes_cnt)]);

					if(pack_key+1 < packs_lenght){
						call_again = true; new_key = ++pack_key;
						break;
					}else{
						var result_arr_length = this._result_arr.length;
						for(var i=0;i<result_arr_length;i++){
							if(this._result_arr[i][1] >0 && check_key==-1){
								this._result_arr[i][1] = parseInt(this._result_arr[i][1]-1);check_key = i;
							}
						}

						if(check_key == -1){
							this._success_flag = false;
						}else{
							call_again = true; new_key = ++check_key;
							break;
						}
					}
				}
			}
		}

		if(call_again){
			if(new_key<packs_lenght){
				this._packetCounter(packs,new_key);
			}else{
				this._success_flag = false;
			}
		}else{
			 this._success_flag = true;
		}
		 
		 return this._success_flag;
	}
	
	/**
	* make _result_arr empty to start the ordering 
	* @return boolean
	*/
	_empty_arr() {
	  this._result_arr = [];return 1;
	}
	
	/**
	* retrive the packs count from packs_arr and put in new array,
	* empty _result_arr,
	* call _packetCounter to start packing the order
	* @count {number} the amount of ordering cookie
	* @packs_arr {array} the array of cookie information
	* like {name: "Croissant", code: "CF", packs: {count: 9, price: '5.95'}}
	*/
	callPacketCounter(count,packs_arr){
		this._count = count;
		let packs_lenght = Object.keys(packs_arr.packs).length;
		const arr = [];
		for(var i=1;i<=packs_lenght;i++){
			arr.push(packs_arr.packs[i].count);
		}
		
		this._empty_arr();
		var my_res=this._packetCounter(arr,0);
	}
	
	setCount(count){
		this._count = count;
	}
	
	
	/**
	@return {array}{boolean} 
	* if ordering was successful returns the array of packes with the amount of them in an array
	* otherwise it returns false
	*/
	getResultArr(){
		switch(this._success_flag){
			case true:
				var final_arr = [];
				for (const [key, value] of this._result_arr) {
					if(value > 0){
						let obj = final_arr.find(o => o.num === key);
						if(typeof obj !== 'undefined'){
							obj.count = parseInt(obj.count)+parseInt(value);
						}else{
							final_arr.push({num:key,count:value});
						}
					}
				}
				return final_arr;
				break;
			case false:
				return false;
				break;
		}
	}
	
}
